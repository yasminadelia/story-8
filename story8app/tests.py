from django.test import TestCase, Client
from django.urls import resolve
from . import views
from .models import *
from .views import *

from django.contrib.staticfiles.testing import StaticLiveServerTestCase
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
import time


# Create your tests here.
class UnitTest(TestCase):
	
	def test_home_url_exist(self):
		response = Client().get('/')
		self.assertEqual(response.status_code, 200)

	def test_home_using_right_template(self):
		response = Client().get('/')
		self.assertTemplateUsed(response, 'story8app.html')

	def test_calling_right_views_function(self):
		found = resolve('/')
		self.assertEqual(found.func, views.index)

	def test_page_is_not_exist(self):
		self.response = Client().get('/NotExistPage/')
		self.assertEqual(self.response.status_code, 404)

	def test_url_book_api_ajax(self):
		response = Client().get("/book_api/")
		self.assertEqual(response.status_code, 200)

class Story8AppFunctionalTest(StaticLiveServerTestCase):

	def setUp(self):
		
		chrome_options = Options()
		chrome_options.add_argument("--disable-dev-shm-usage")
		chrome_options.add_argument('--dns-prefetch-disable')
		chrome_options.add_argument('--no-sandbox')        
		chrome_options.add_argument('--headless')
		chrome_options.add_argument('disable-gpu')


		self.selenium  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
		super(Story8AppFunctionalTest, self).setUp()

	def tearDown(self):
		self.selenium.quit()
		super(Story8AppFunctionalTest, self).tearDown()

	def test_book_search(self):
		selenium = self.selenium
        # Opening the link we want to test
		selenium.get('http://localhost:8000/')
		time.sleep(5) 

		input_form = selenium.find_element_by_id('search-input')
		input_form.send_keys('test')

		selenium.find_element_by_id('search-button').click()

		time.sleep(10)
		result = selenium.find_element_by_id('book_list')
		self.assertIn('test', result.get_attribute('innerHTML').lower())

		input_form.clear()
		input_form.send_keys('book of happiness')
		selenium.find_element_by_id('search-button').click()

		time.sleep(10)
		result = selenium.find_element_by_id('book_list')
		self.assertNotIn('test', result.get_attribute('innerHTML').lower())
		self.assertIn('book of happiness', result.get_attribute('innerHTML').lower())



